export const environment = {
    server: { port: process.env.SERVER_PORT || 5000 },
    db: {url: process.env.DB_URL || 'mongodb://orange:k1r4nd14@ds237574.mlab.com:37574/orangesantos'},
    security: { saltRounds: process.env.SALT_ROUNDS || 10 }
}
