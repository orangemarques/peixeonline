module.exports = {
  apps : [{
			name   : "peixeonline",
			script : "./dist/main.js",
			instances: 0,
			exec_mode: "cluster",
			env: {
					SERVER_PORT: 5000,
					DB_URL: "mongodb://orange:k1r4nd14@ds237574.mlab.com:37574/orangesantos",
					NODE_ENV: "production"
			}
  }]
}
