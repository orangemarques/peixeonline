import {Server} from './server/server'
import {produtosRouter} from './produtos/produtos.router'

const server = new Server()

server.bootstrap([
    produtosRouter,

]).then(server=>{
  console.log('Server is listening on:', server.application.address())

}).catch(error=>{
  console.log('Server failed to start')
  console.error(error)
  process.exit(1)
})
