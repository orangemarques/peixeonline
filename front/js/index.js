const _content = document.getElementById("content");
const _popup = document.getElementById("popup_produto");
const _form = document.getElementById("form_produto");
const ajaxRequest = new XMLHttpRequest();

window.addEventListener("hashchange", (event) => {
    const _page = event.newURL.split('#')[1];
    page(_page);
})

document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();

    let peixe = {};

    peixe.nome = _form.nome.value;
    peixe.descricao = _form.descricao.value;
    peixe.preco = _form.preco.value;

    if (_form.id_peixe.value == 0) {
        ajaxRequest.open("POST", "http://localhost:5000/produtos", true);

    } else {
        ajaxRequest.open("PUT", `http://localhost:5000/produtos/${_form.id_peixe.value}`, true);
    }

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState != 4) return;

        if (ajaxRequest.status == 200) {

            if (_form.id_peixe.value == 0) {
                alert("Peixe inserido com sucesso!");

            } else {
                alert("Peixe atualizado com sucesso!");
            }

            page("produtos");
            _form.reset();
            closePopup();

        } else if (ajaxRequest.status >= 400) {
            if(ajaxRequest === null || ajaxRequest === undefined) {
                alert("Ocorreu um erro inexperado, tente novamente mais tarde!");
                return;
            } 

            response = JSON.parse(ajaxRequest.response);

            if(response.message !== null && response.message !== undefined) {
                alert(response.message);
            }
        }
    }

    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send(JSON.stringify(peixe));
});

document.querySelector("#b_cancelar").addEventListener("click", () => {
    closePopup();
});

closePopup = function() {
    _popup.style.display = "none";
}

openPopup = function(id = 0, dados = {}) {
    if (id !== 0) {
        _form.nome.value = dados.nome;
        _form.descricao.value = dados.descricao;
        _form.preco.value = dados.preco;
    }

    _form.id_peixe.value = id;
    _popup.style.display = "block";
}

page = function(target) {
    _content.innerHTML = "";

    if(target.includes("produtos")) {
        getItems(_content);

    } else if(target.includes("sobre")) {
        _content.innerHTML += `
        <p>
            Sistema para vender seu peixe.
        </p>
        `;

    } else if(target.includes("creditos")) {
        _content.innerHTML += `
        <p>
            <ul>
                <li>desenvolvido por: <b>Orange Santos</b></li>
                <li>Backend: <b>Node</b></li>
                <li>Frontend: <b>Vanilla JS</b></li>
            </ul>
        </p>
        `;
    }

    if(target.includes("incluir")) {
        addItem();

    } else if(target.includes("editar")) {
        const _target = target.split('=')[1];
        editItem(_target);

    } else if(target.includes("deletar")) {
        const _target = target.split('=')[1];
        deleteItem(_target);

    } else {
        //_content.innerHTML = "";
    }

}

getItems = function(content) {
    let data = [];

    ajaxRequest.open("GET", "http://localhost:5000/produtos", true);

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState != 4) return;

        if (ajaxRequest.status == 200) {
            data = JSON.parse(ajaxRequest.responseText);
            paserItems(content, data.items);

        } else if (ajaxRequest.status >= 400) {
            if(ajaxRequest === null || ajaxRequest === undefined) {
                alert("Ocorreu um erro inexperado, tente novamente mais tarde!");
                return;
            } 

            response = JSON.parse(ajaxRequest.response);

            if(response.message !== null && response.message !== undefined) {
                alert(response.message);
            }
        }
    }

    ajaxRequest.send();
}

paserItems = function(content, items) {
    content.innerHTML += `
    <a href="#produtos/incluir">[ INCLUIR NOVO PEIXE ]</a><br><br>
    <table border="1">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Preço</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="items">
        </tbody>
    </tavle>
    `;

    const _items = document.getElementById("items");

    items.forEach(element => {
        _items.innerHTML += `
            <tr>
                <td><span id="nome_${element._id}">${element.nome}</span></td>
                <td><span id="descricao_${element._id}">${element.descricao}</span></td>
                <td style="text-align:right"><span id="preco_${element._id}">${element.preco.toFixed(2)}</span></td>
                <td><a href="#produtos/editar=${element._id}">[ EDITA PEIXE ]</a> <a href="#produtos/deletar=${element._id}">[ DELETA PEIXE ]</a></td>
            </tr>
        `;
    });
}

deleteItem = function(id) {
    ajaxRequest.open("DELETE", `http://localhost:5000/produtos/${id}`, true);

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState != 4) return;

        if (ajaxRequest.status === 204) {
            alert("Peixe removido com sucesso!");
            page("produtos");

        } else if (ajaxRequest.status >= 400) {
            if(ajaxRequest === null || ajaxRequest === undefined) {
                alert("Ocorreu um erro inexperado, tente novamente mais tarde!");
                return;
            } 

            if(response.message !== null && response.message !== undefined) {
                alert(response.message);
            }
        }
    }

    ajaxRequest.send();
}

editItem = function(id) {
    setTimeout(() => {
        _nome = document.getElementById(`nome_${id}`).innerText;
        _descricao = document.getElementById(`descricao_${id}`).innerText;
        _preco = document.getElementById(`preco_${id}`).innerText;

        dados = {
            "nome": _nome,
            "descricao": _descricao, 
            "preco": _preco 
        }

        openPopup(id, dados);
    }, 500);
}

addItem = function() {
    openPopup();
}