import * as mongoose from 'mongoose'

export interface Produto extends mongoose.Document {
    nome: string,
    descricao: string,
    preco: number
}

const produtoSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    },
    preco: {
        type: Number,
        required: true
    }
})

export const Produto = mongoose.model<Produto>('Produto', produtoSchema)
