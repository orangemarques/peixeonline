import {ModelRouter} from '../common/model-router'
import * as restify from 'restify'
import {Produto} from './produtos.model'

class ProdutosRouter extends ModelRouter<Produto> {
    constructor(){
        super(Produto)
    }

    applyRoutes(application: restify.Server){
        application.get('/produtos', this.findAll)
        application.get('/produtos/:id', [this.validateId, this.findById])
        application.post('/produtos', this.save)
        application.put('/produtos/:id', [this.validateId,this.replace])
        application.patch('/produtos/:id', [this.validateId,this.update])
        application.del('/produtos/:id', [this.validateId,this.delete])
    }

}

export const produtosRouter = new ProdutosRouter()
