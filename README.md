![CSCore Logo](http://www.hpsindicos.com.br/img/peixeonline.jpg)

# Peixe Online - Venda seu peixe #

### Tecnologias usadas no Backend ###
 - **NODE**
 - **Mongodb** 
 - **Typescript**


Para executar o backend no seu computador:

 - Instale o node

        https://nodejs.org/en/download/

 - Não precisa instalar o mongo, estamos usando um banco nas nuvens

 - Execute o npm install

        npm install

- Buildar nosso servidor rest

        npm run build

- Startar nosso servidor rest

        node ./dist/main


Após startar o backend basta abrir nosso index.html que está na pasta /front


Qualquer dúvida, fovor entrar em contato, obrigado